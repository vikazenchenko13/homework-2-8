set_1 = {5, 7, 55, 68, 74, 2, 4, 8}
set_2 = {x for x in range(10)}

set_3 = set_1.intersection(set_2)
print("Set 1:", set_1)
print("Set 2:", set_2)
print("Common elements of sets:", set_3)
